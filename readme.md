# Nb++

## motivation
Nb++ stands for "nobuild++". Nobuild (currently "nob") is a build system for the C programming language written in... C!
This project is a *slight* improvement over Nob. My main issue with Nob is that it's source code is quite big for it's complexity.
~1K LOC is not too much. but a lot of the code is just implementing structures and macros that Libc should provide, but doesn't.
This issue can be solved by using C++ instead of C, which allows us to use things like `std::vector` or `std::string` instead of writing
our own dynamic arrays and strings + we can easily plug in our own allocator.

Many consider C++ bloated, but it's really up to the user how much they want to bloat their code. The language has a lot of nice features
that make writing code easier, but the user isn't obligated to using them - they can pick and choose what they like and what they don't.
This is one of the reasons why although Nb++ is a C++ project, it manages to stay away from the bloaty nature of C++.

Nb++ is not a fork of Nob nor will it be merged into it. Nb++ aims to be a separate project that takes the C++ spin on the original idea of Nob
and implements it's features + adds new functionallity. Nob is by no means better or worse that Nb++, it's just the same idea presented differently.
If you feel like Nb++ is too much for you or you just prefer C or need better performance, then Nob would be a better fit for you.
NB++ IS BY ALL MEANS NOT A COMPETITOR TO NOB!

## usage

First, you need to bootstrap the build system:

```console
g++ nb++.cpp -o nb++ -std=20 # -std=20 is needed here
```

The build system can now recompile itself:

```console
./nb++ # if this fails, revert to the previous successful build
```

## examples

A working example can be found in `examples/dwm/nb++.cpp`. This program demonstrates how to compile a simple C project with Nb++.

## credits
- Nobuild: https://github.com/tsoding/nobuild.git
- nob.h: https://github.com/tsoding/musializer/blob/master/nob.h
- Tsoding's GH: https://github.com/tsoding
- Tsoding's YT: https://www.youtube.com/@TsodingDaily
