#ifndef NBPP_CACHE_H_
#define NBPP_CACHE_H_

#include <vector>
#include <string>
#include <tuple>
#include <ctime>

namespace _nb_cache {
std::vector<std::tuple<std::string, std::time_t>> cache = {
};
}
#endif // NBPP_CACHE_H_
