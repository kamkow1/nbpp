#include <iostream>
#include <vector>
#include <string>
#include <utility>

#define CONFIGURATION "examples/zig-simple/nb++-configuration.hpp"
#define NBPP_CACHE "examples/zig-simple/nb++-cache.hpp"
#include "../../nb++.hpp"

enum SubcommandIndex
{
	SBI_BUILD,
	SBI_RUN,
};

static const char *subcommands[] =
{
	[SBI_BUILD]   = "build",
	[SBI_RUN]     = "run",
};

int main(int argc, char **argv)
{
	std::tuple<bool, nb::OptMap> opts_result = nb::parse_options(argc, argv);
	bool ok = std::get<bool>(opts_result);
	if (!ok) {
		nb::log_error("failed to parse commandline options");
		return 1;
	}
	nb::OptMap optmap = std::get<nb::OptMap>(opts_result);
	nb::Configuration configuration = nb::my_configuration; // defined in nb++-configuration.hpp

	// C++	
	if (optmap.contains("c++")) {
		std::string cxx = std::get<std::string>(optmap["c++"]);
		std::vector<std::string> search_paths = {
			// my copy of Zig
			"/archive/ARCHIVE/zig-bootstrap-0.10.1/zig-bootstrap-0.10.1/out/host/bin/"
		};
		std::tuple<bool, std::string> recom_compiler = nb::find_program(cxx, search_paths);
		if (!std::get<0>(recom_compiler)) {
			nb::log_error("recom_compiler: not found " + cxx);
			return 1;
		}
		configuration.recom_compiler = std::get<1>(recom_compiler);
	} else nb::log_warning("No C++ compiler selected. Defaulting to " + configuration.recom_compiler);
			
	if (optmap.contains("zig")) {
		std::string zig = std::get<std::string>(optmap["zig"]);
		std::tuple<bool, std::string> compiler = nb::find_program(zig, {});
		if (!std::get<0>(compiler)) {
			nb::log_error("compiler: not found " + zig);
			return 1;
		}
		configuration.compiler = std::get<1>(compiler);
	} else nb::log_warning("No Zig compiler selected. Defaulting to " + configuration.compiler);
			
	if (optmap.contains("ld")) {
		std::string ld = std::get<std::string>(optmap["ld"]);
		std::tuple<bool, std::string> linker = nb::find_program(ld, {});
		if (!std::get<0>(linker)) {
			nb::log_error("linker: not found " + ld);
			return 1;
		}
		configuration.linker = std::get<1>(linker);
	} else nb::log_warning("No linker selected. Defaulting to " + configuration.linker);

	if (optmap.contains("sc")) {
		std::string subcmd = std::get<std::string>(optmap["sc"]);
		nb::log_info("Running subcommand: " + subcmd);
		if (subcmd == subcommands[SBI_BUILD]) {
			std::tuple<bool, bool> result = nb::check_if_changed();
			if (!std::get<0>(result)) return 1; // nb will log the internal error

			if (std::get<0>(result) && std::get<1>(result)) {
				if (!nb::recompile_self(argc, argv, configuration)) {
					nb::log_error("nb++ failed to recompile itself");
					return 1;
				} else nb::rerun_self(argc, argv);
			}

			std::vector<std::string> src = nb::src(".zig", {});

			// basic
			nb::Flags compile_flags;
			compile_flags.push(
				"-femit-bin=#object#.o"
			);

			std::vector<std::string> obj = nb::compile(src, configuration, compile_flags);

			nb::Flags link_flags;
			nb::link(obj, configuration, link_flags);
		} else if (subcmd == subcommands[SBI_RUN]) {
			if (!nb::run_cmd("./main")) return 1;
		}
	} else {
		nb::log_error("Please select a subcommand to run using `-sc` option");
		nb::log_info("Available subcommands:");
		for (size_t i = 0; i < sizeof(subcommands)/sizeof(subcommands[0]); ++i) nb::log_info(subcommands[i]);

		return 1;
	}

	return 0;
}
