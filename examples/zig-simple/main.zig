// SOURCE: https://raw.githubusercontent.com/runeanielsen/ascii-table-zig/master/src/main.zig
// Credits: runeanielsen

const std = @import("std");
const stdout = std.io.getStdOut().writer();
const fmt = std.fmt;
const mem = std.mem;
// Normally this wouldn't make sense, because this program is reallt simple
// but we need to test working with multiple translation units
const table = @import("table.zig");

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    const generated_table = try table.asciiTable(allocator);
    try stdout.print("{s}\n", .{generated_table});
}
