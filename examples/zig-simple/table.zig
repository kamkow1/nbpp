const std = @import("std");
const fmt = std.fmt;
const mem = std.mem;

const headerRow: []const u8 = ("Dec  Hex  Oct  C" ++ " | ") ** 3 ++ "Dec  Hex  Oct  C";

fn getChar(i: u8) u8 {
    return if (i < 33 or i == 127) ' ' else i;
}

fn tableRows() [32][4]u8 {
    var rows: [32][4]u8 = undefined;
    var i: u8 = 0;
    while (i <= 31) : (i += 1) {
        rows[i][0] = i;
        rows[i][1] = i + 32;
        rows[i][2] = i + 64;
        rows[i][3] = i + 96;
    }

    return rows;
}

fn bodyRow(allocator: mem.Allocator, tableRow: [4]u8) ![]const u8 {
    var formattedBlocks: [4][]const u8 = undefined;
    for (tableRow) |n, i| {
        formattedBlocks[i] = try fmt.allocPrint(
            allocator,
            "{d:>3} {x:>4} {o:>4}  {c}",
            .{ n, n, n, getChar(n) },
        );
    }

    return try mem.join(allocator, " | ", &formattedBlocks);
}

pub fn asciiTable(allocator: mem.Allocator) ![]const u8 {
    var formattedTableRows: [33][]const u8 = undefined;
    formattedTableRows[0] = headerRow;
    for (tableRows()) |row, i| {
        formattedTableRows[i + 1] = try bodyRow(allocator, row);
    }

    return try mem.join(allocator, "\n", formattedTableRows[0..]);
}
